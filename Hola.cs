//parece ser que csharp non
//importa nada pordefecto como en java java.lang.*
//en csharp temos os namespaces en contraposicion os packages de java
//non necesitan un directorio fisico que sigan a nomenclatura do namespacce
//como en java os packages 
//xa que � un executable xa o compila e fai a ensamblaxe e lincado
//non facemos usos de classpath
//pero se � un assembly????
//pero si � unha boa practica
using System;

//o using o igual que o import so serve para os namespace
//os namespace o igual que a directiva package en java
//ten a funcion de evitar colision de compo�entes
//a diferencia que en java nun mesmo arquivo poden convivir varios
//namespace
namespace Hola
{
public class Hola
{
    //o main ten que estar en maiusculas a primeira letra
    //non ten porque ser public e podemos devolver un int coma en c ou c++
    //e os argumentos son opcionais
    static int Main(){
        //e se devolve un int temos que indicalo explicitamente
        System.Console.WriteLine("hola mundo");
        return 0;
    }
}
}
